<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pages')->insert([
            'title' => 'Hello World',
            'content' => 'Integer posuere erat posuere velit aliquet.',
        ]);
        DB::table('pages')->insert([
            'title' => 'Hello2',
            'content' => 'Aenean eu leo quam uam venenatis vestibulum.',
        ]);
    }
}

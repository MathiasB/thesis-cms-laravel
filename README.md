Thesis CMS Laravel
------------------

PHP/Laravel version of the test CMS used for the thesis.


Installation
------------

This Laravel app requires PHP 7.1 and Composer.  
You can install it using composer:

    $ php composer install


Usage
-----

    $ php artisan serve


This serves the app at `localhost:8000`.

To test the client-side rendered version, you can reuse the Vue app from the [Go version](https://bitbucket.org/MathiasB/thesis-cms-go).


Author
------

[Mathias Beke](https://denbeke.be)
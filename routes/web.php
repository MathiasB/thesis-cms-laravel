<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Page;

Route::get('/', function () {
    return view('index', [
        'pages' => App\Page::all(),
    ]);
});

Route::get('/page/{id}', function ($id) {
    return view('page', [
        'pages' => App\Page::all(),
        'page' => App\Page::find($id),
    ]);
});

Route::get('/api/pages', function () {
	return response()->json(App\Page::all());
});

Route::get('/api/page/{id}', function ($id) {
	return response()->json(App\Page::find($id));
});

